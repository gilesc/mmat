import os

from .core import MMAT

class MatrixIOError(Exception):
    pass

class Matrix(object):
    """
    Stores a matrix, its transposition, and possibly row, column, and general metadata.
    """
    def __init__(self, key, root):
        self.root = root
        self.key = key

        files = os.listdir(root)
        if not (("0.mmat" in files) and ("1.mmat" in files)):
            raise MatrixIOError(f"Invalid matrix directory: {root}")
        self._X0 = MMAT(os.path.join(root, "0.mmat"))
        self._X1 = MMAT(os.path.join(root, "1.mmat"))

    def validate(self):
        #TODO: validate the directory is correct, X0 match with X1, etc
        pass

    def index(self):
        return self._X0.index.tolist()

    def columns(self):
        return self._X0.columns.tolist()

    def shape(self):
        return self._X0.shape

    def get_rows(self, keys):
        return self._X0.loc[keys,:].to_frame()

    def get_columns(self, keys):
        return self._X1.loc[keys,:].to_frame().T

        
class MatrixStore(object):
    def __init__(self, root):
        self.root = root
        self._items = {}

        for root, dirs, files in os.walk(self.root):
            try:
                p = os.path.relpath(root, self.root)
                self._items[p] = Matrix(p, root)
            except MatrixIOError:
                continue

    def keys(self):
        return list(self._items.keys())

    def __getitem__(self, key):
        return self._items[key]
