import codecs
import io
import base64
import pickle
import zlib

import pandas as pd
import click
import bottle

from .store import MatrixStore

store = None
app = bottle.Bottle()

def encode_response(item):
    response = {
        "type": "json",
        "data": item
    }
    if isinstance(item, pd.DataFrame):
        response["data"] = base64.b64encode(zlib.compress(pickle.dumps(item))).decode("utf-8")
        response["type"] = "pickle"
    return response

@app.post("/")
def API():
    query = bottle.request.json
    ns = query["namespace"]
    fn_name = query["method"]
    params = query["parameters"]
    print(query)
    
    if query["namespace"] == "store":
        fn = getattr(store, fn_name)
        if query["type"] == "function":
            o = fn(**params)
        elif query["type"] == "property":
            o = fn()
        else:
            raise
    elif query["namespace"] == "matrix":
        matrix_key = query["matrix_key"]
        matrix = store[matrix_key]
        fn = getattr(matrix, fn_name)
        if query["type"] == "function":
            o = fn(**params)
        elif query["type"] == "property":
            o = fn()
        else:
            raise
    else:
        pass

    return {"payload": encode_response(o)}

@click.command()
@click.argument("matrix_directory", nargs=1)
@click.option("--host", default="0.0.0.0")
@click.option("--port", default=12751, type=int)
def main(matrix_directory, host, port):
    global store
    store = MatrixStore(matrix_directory)
    print(store._items.keys())
    bottle.run(app, host=host, port=port)

if __name__ == "__main__":
    main()
