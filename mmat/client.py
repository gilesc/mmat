import json
import functools
import base64
import io
import zlib
import pickle
import logging

import pandas as pd
import requests

logging.getLogger("urllib3").setLevel(logging.WARNING)

def decode_response(response):
    if response["type"] == "pickle":
        return pickle.loads(zlib.decompress(base64.b64decode(response["data"])))
    else:
        return response["data"]

def make_wrapper(type_name):
    def wrapper(fn):
        @functools.wraps(fn)
        def wrap(obj, *args, **kwargs):
            params = fn(obj, *args, **kwargs) or {}
            payload = {
                "method": fn.__name__,
                "type": type_name, 
                "parameters": params
            }
            if isinstance(obj, StoreProxy):
                payload["namespace"] = "store"
            elif isinstance(obj, MatrixProxy):
                payload["namespace"] = "matrix"
                payload["matrix_key"] = obj.key
            return obj._make_request(payload)
        if type_name == "property":
            wrap = property(wrap)
        return wrap
    return wrapper

proxy_function = make_wrapper("function")
proxy_property = make_wrapper("property")

class Proxy(object):
    def __init__(self, host="localhost", port=12751):
        self.host = host
        self.port = port

    @property
    def URI(self):
        return f"http://{self.host}:{self.port}/"

    def _make_request(self, params):
        headers = {"Content-Type": "application/json"}
        payload = params
        response = requests.post(self.URI, data=json.dumps(payload), headers=headers)
        return decode_response(response.json()["payload"])

class StoreProxy(Proxy):
    @proxy_function
    def keys(self):
        return

    def __getitem__(self, key):
        if not key in self.keys():
            raise KeyError
        return MatrixProxy(key, host=self.host, port=self.port)

class MatrixProxy(Proxy):
    def __init__(self, key, host, port):
        self.key = key
        super().__init__(host=host, port=port)

    @proxy_property
    def shape(self):
        return

    @proxy_property
    def columns(self):
        return

    @proxy_property
    def index(self):
        return

    @proxy_function
    def get_rows(self, keys):
        return {"keys": keys}

    def get_row(self, key):
        return self.get_rows([key]).iloc[:,0]

    @proxy_function
    def get_columns(self, keys):
        return {"keys": keys}

    def get_column(self, key):
        return self.get_columns([key]).iloc[:,0]

Client = StoreProxy

if __name__ == "__main__":
    proxy = StoreProxy()
    X = proxy["ARCHS4/9606"]
    print(X.index[:5])
    print(X.get_rows(X.index[:5]))
