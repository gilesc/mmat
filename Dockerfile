FROM ubuntu:20.04

ENV PYTHONUNBUFFERED 1

VOLUME ["/data"]

RUN apt-get update
RUN apt-get upgrade -y
RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y python3-pip git g++ curl
RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y python3-numpy python3-pandas

WORKDIR /script
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

EXPOSE 12751

COPY mmat mmat

CMD ["python3", "-m", "mmat.server", "/data"]
