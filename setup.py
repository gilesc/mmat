from setuptools import setup, find_packages

setup(
    name="mmat",
    version="0.1.0",
    description="Memory-mapped matrix storage",
    author="Cory Giles",
    author_email="gilesc@omrf.org",
    packages=find_packages(),
    install_requires=["click", "bottle", "pandas", "numpy", "requests"]
)
